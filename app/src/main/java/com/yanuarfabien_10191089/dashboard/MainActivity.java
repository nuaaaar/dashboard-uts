package com.yanuarfabien_10191089.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        String[] texts = new String[]{
                "My Account",
                "Inventory",
                "Analytics",
                "Messages"
        };

        int[] images = new int[]{
                R.drawable.ic_baseline_account_box_24,
                R.drawable.ic_baseline_archive_24,
                R.drawable.ic_baseline_insert_chart_24,
                R.drawable.ic_baseline_chat_24
        };

        RvAdapter adapter = new RvAdapter(this, images, texts);

        rv = findViewById(R.id.rv_menu);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new GridLayoutManager(this, 2));

    }
}